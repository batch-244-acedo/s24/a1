// Item 1
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

// Item 2
let address = [258, "Washington Ave NW", "California", 90011];

let [houseNumber, street, state, zip] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zip}`);

// Item 3
let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
};

let {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

// Item 4
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number))
	console.log(numbers.reduce((x, y) => x + y));



// Item 5
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog ("Frankie", 5, "Miniature Dachsund");
console.log(myDog);